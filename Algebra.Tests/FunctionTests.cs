using System;
using NUnit.Framework;
using static Algebra.Functions;

namespace Algebra.Tests
{
    [TestFixture]
    public class FunctionTests
    {
        [TestCase(-13.45, 13.45, 0.00001)]
        [TestCase(-1000.12, 1000.12, 0.00001)]
        [TestCase(1, 1, 0.00001)]
        [TestCase(1.2, 1.44,0.00001)]
        [TestCase (2, 4, 0.00001)]
        [TestCase(78.45, 4, 0.00001)]
        public void Tests(double x, double expected, double accuracy)
        {
            double actual = F(x);
            Assert.IsTrue(Math.Abs(actual - expected) <= accuracy);
        }
    }
}