using Microsoft.AspNetCore.Builder;

var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

app.MapGet("/f/{x:double}", (double x) => $"F({x}) = {Algebra.Functions.F(x)}");

app.Run();